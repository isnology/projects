class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.string :name
      t.date :start_date
      t.date :finish_date
      t.references :user, foreign_key: true

      t.timestamps
      t.integer :lock_version
    end
  end
end
