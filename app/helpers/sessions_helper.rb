module SessionsHelper
  
  def my_user
    if (user_id = session[:user_id])
      @my_user ||= User.find_by(id: user_id)
    else
      @my_user = nil
    end
  end

  # logs out the user
  def log_out
    forget(my_user)
    session.delete(:user_id)
  end
  
  # logs in the user.
  def log_in(user)
    session[:user_id] = user.id
  end
  
  # returns true if the user is logged in, false otherwise.
  def logged_in?
    !my_user.nil?
  end
  
  # Remember a user in a persistent session
  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end

  # Forgets a persistent session.
  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end
  
end
