class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  
  protected

    # skip login and signup if logged in already
    def check_logged_in
      redirect_to '/projects' if logged_in?
    end
  
end
