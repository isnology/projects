require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  
  before :each do

  end
  
  describe "GET #new" do
    fixtures :users
    
    it "returns http success" do
      @user = users(:michael)
      @other_user = users(:archer)
      get :new
      expect(response).to have_http_status(:success)
    end
  end

  describe 'should redirect index when not logged in' do
    it 'redirects to login' do
      get project_path
      expect(response).to redirect_to(login_url)
    end
  end
  
end
