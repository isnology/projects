require 'rails_helper'

RSpec.describe User, type: :model do
  
  before :each do
    @user = User.new(name: "Example User", email: "user@example.com", password: "foobar",
                     password_confirmation: "foobar")
  end
  
  describe 'object' do
    it 'should be valid' do
      expect(@user).to be_valid
    end
    
    it 'name is not present' do
      @user.name = "     "
      expect(@user).not_to be_valid
    end
    
    it 'email is not present' do
      @user.email = "     "
      expect(@user).not_to be_valid
    end
    
    it 'name is too long' do
      @user.name = 'a' * 51
      expect(@user).not_to be_valid
    end
    
    it 'email is too long' do
      @user.email =  'a' * 244 + '@example.com'
      expect(@user).not_to be_valid
    end
    
    it 'email address is valid' do
      valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
      valid_addresses.each do |valid_address|
        @user.email = valid_address
        expect(@user).to be_valid, "#{valid_address.inspect} is invalid"
      end
    end
    
    it 'email address is invalid' do
      invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
      invalid_addresses.each do |invalid_address|
        @user.email = invalid_address
        expect(@user).not_to be_valid, "#{invalid_address.inspect} is valid"
      end
    end
    
    
    it 'password is blank' do
      @user.password = @user.password_confirmation = ' ' * 6
      expect(@user).not_to be_valid
    end
    
    it 'password is less than minimum length' do
      @user.password = @user.password_confirmation = 'a' * 5
      expect(@user).not_to be_valid
    end
    
  end

  describe 'being saved' do
    it 'is not unique' do
      duplicate_user = @user.dup
      @user.save
      expect(duplicate_user).not_to be_valid
    end
    
    it 'has email address that is not unique' do
      duplicate_user = @user.dup
      duplicate_user.email.upcase!
      @user.save
      expect(duplicate_user).not_to be_valid
    end
  end
  
end
